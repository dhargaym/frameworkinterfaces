﻿// See https://aka.ms/new-console-template for more information
using FrameworkInterfacesDemo;

List<Player> players = new List<Player>();
players.Add(new Player() { PlayerId = 10, PlayerName = "Abc" }); //using Object Initializer syntax to create player instances
players.Add(new Player() { PlayerId = 1, PlayerName = "Sdf" }); //using Object Initializer syntax to create player instances
players.Add(new Player() { PlayerId = 20, PlayerName = "Wer" }); //using Object Initializer syntax to create player instances
players.Add(new Player() { PlayerId = 15, PlayerName = "Ghy" }); //using Object Initializer syntax to create player instances
players.Sort();
foreach (Player player in players)
{
    Console.WriteLine(player);
}


List<int> numbers= new List<int>();
numbers.Add(10);
numbers.Add(2);
numbers.Add(300);
numbers.Add(30); 
numbers.Add(30);
numbers.Sort();

foreach(int number in numbers)
{
    Console.WriteLine(number);
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrameworkInterfacesDemo
{
    internal class Player : IComparable<Player>
    {
        public int PlayerId { get; set; }
        public string PlayerName { get; set; }

        public int CompareTo(Player? other)
        {
            if (other == null) return 1;
            else if (this.PlayerId > other.PlayerId)
                return 1; //invoking object is larger than the parameter
            else if (this.PlayerId == other.PlayerId)
                return 0; //invoking object is the same as the parameter
            else 
                return -1;//invoking object is smaller than the parameter
        }

        public override string ToString() => $"{PlayerId}, {PlayerName}";
    }
}
